<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function __invoke()
    {
        return view('auth.login');
    }

    public function login(LoginRequest $request)
    {
        $formData = $request->validated();

        if (!Auth::attempt($formData, $request->boolean('remember'))) {
            return redirect()->route('login')->withErrors(['message' => 'its not true']);
        }
        return redirect()->route('home');
    }

}
