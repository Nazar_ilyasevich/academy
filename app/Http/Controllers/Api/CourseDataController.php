<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Course;
use Illuminate\Http\Request;

class CourseDataController extends Controller
{
    public function getDataCourse($course)
    {
        $courseData = Course::getDataFromOneCourse($course);

        echo json_encode($courseData);
    }
}
