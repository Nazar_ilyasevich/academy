<?php

namespace App\Http\Controllers\Design;

use App\Http\Controllers\Controller;
use App\Http\Requests\Store\BackendRequest;
use App\Http\Requests\Store\DesignRequest;
use App\Http\Requests\Store\FrontendRequest;
use App\Models\Course;
use Illuminate\Http\Request;

class StoreController extends Controller
{
    private string $course = 'design';
    public function __invoke(DesignRequest $request)
    {
        Course::setDataForCourse($request->validated(), $this->course);

        return redirect()->route('design.index');
    }
}
