<?php

namespace App\Http\Controllers\Design;

use App\Http\Controllers\Controller;
use App\Models\Course;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function __invoke()
    {
        $course = Course::getDataFromOneCourse('design');
        return view('design.index', compact('course'));
    }
}
