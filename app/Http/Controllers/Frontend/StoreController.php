<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Store\BackendRequest;
use App\Http\Requests\Store\FrontendRequest;
use App\Models\Course;
use Illuminate\Http\Request;

class StoreController extends Controller
{
    private string $course = 'frontend';
    public function __invoke(FrontendRequest $request)
    {
        Course::setDataForCourse($request->validated(), $this->course);

        return redirect()->route('frontend.index');
    }
}
