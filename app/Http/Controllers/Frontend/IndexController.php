<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Course;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function __invoke()
    {
        $course = Course::getDataFromOneCourse('frontend');
        return view('frontend.index', compact('course'));
    }
}
