<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Course;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class IndexController extends Controller
{
    public function __invoke()
    {
        $course = Course::getDataFromOneCourse('backend');

        return view('backend.index', compact('course'));
    }
}
