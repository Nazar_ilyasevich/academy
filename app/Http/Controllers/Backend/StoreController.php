<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Store\BackendRequest;
use App\Models\Course;
use Illuminate\Http\Request;

class StoreController extends Controller
{
    private string $course = 'backend';

    public function __invoke(BackendRequest $request)
    {
        Course::setDataForCourse($request->validated(), $this->course);

        return redirect()->route('backend.index');
    }
}
