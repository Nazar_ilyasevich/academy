<?php

namespace App\Http\Requests\Store;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class BackendRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'webinar_time' => ['required', 'date_format:H:i:s'],
            'webinar_date' => ['required', 'date'],
            'start_course' => ['required', 'date'],
            'schedule'     => ['required', 'string']
        ];
    }


    protected function prepareForValidation() //до валідації
    {
        $this->merge([
            'webinar_time' => date('H:i:s', strtotime(str_replace('-', '/', $this->get('webinar_time')))),
            'webinar_date' => date('Y-m-d', strtotime(str_replace('-', '/', $this->get('webinar_date')))),
            'start_course' => date('Y-m-d', strtotime(str_replace('-', '/', $this->get('start_course')))),
        ]);
    }
}
