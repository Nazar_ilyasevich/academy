<?php

namespace App\Models;

use Carbon\Exceptions\UnknownSetterException;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Course extends Model
{
    use HasFactory;

    protected $fillable = [
        'course',
        'webinar_time',
        'webinar_date',
        'start_course',
    ];

    public static function setDataForCourse($data, string $course): void
    {
        Course::query()
            ->where('course', $course)
            ->update($data);
    }




    public static function getDataFromOneCourse(string $course)
    {
        $data = Course::query()->select([
                'course',
                'webinar_time',
                'webinar_date',
                'start_course',
                'schedule',
            ])
            ->where('course', $course)
            ->first();


        return $data;
    }

}
