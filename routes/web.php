<?php


use Illuminate\Support\Facades\Route;


Route::get('/', function () {
    return redirect()->route('backend.index');
})->name('home');


Route::get('/login', App\Http\Controllers\Auth\LoginController::class)->name('login');
Route::post('/login_process', [App\Http\Controllers\Auth\LoginController::class, 'login'])->name('login_process');

Route::get('/register', App\Http\Controllers\Auth\RegisterController::class)->name('register');
Route::post('/register_process', [App\Http\Controllers\Auth\RegisterController::class, 'register'])->name('register_process');


Route::middleware('auth')->prefix('auth')->group(function () {
    Route::get('/logout', App\Http\Controllers\Auth\LogoutController::class)->name('logout');

    Route::get('backend', App\Http\Controllers\Backend\IndexController::class)->name('backend.index');
    Route::patch('backend/store', App\Http\Controllers\Backend\StoreController::class)->name('backend.store');

    Route::get('frontend', \App\Http\Controllers\Frontend\IndexController::class)->name('frontend.index');
    Route::patch('frontend/store', \App\Http\Controllers\Frontend\StoreController::class)->name('frontend.store');

    Route::get('design', \App\Http\Controllers\Design\IndexController::class)->name('design.index');
    Route::patch('design/store', \App\Http\Controllers\Design\StoreController::class)->name('design.store');
});



