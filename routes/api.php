<?php

use App\Http\Controllers\Api\CourseDataController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;





Route::get('course/{course}', [CourseDataController::class, 'getDataCourse'])->name('course.show');



