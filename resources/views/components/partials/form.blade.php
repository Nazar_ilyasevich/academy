

<div class = "flex flex-wrap my-5 mx-2">
    <div class = "w-full md:w-1/3 lg:w-1/3 p-3">
        <div class = "flex items-center flex-row w-full rounded-md p-3">
            <div class="p-1 text-sm text-blue-700 shadow-md rounded-lg" role="alert">
                <div id="timepicker-value" class="relative xl:w-96" data-te-input-wrapper-init>
                    <input value="{{ $course->webinar_time ?? '' }}" required name="webinar_time" type="text" class="peer block min-h-[auto] w-full rounded border-0 bg-transparent py-[0.32rem] px-3 leading-[2.15] outline-none transition-all duration-200 ease-linear focus:placeholder:opacity-100 data-[te-input-state-active]:placeholder:opacity-100 motion-reduce:transition-none dark:text-neutral-200 dark:placeholder:text-neutral-200 [&:not([data-te-input-placeholder-active])]:placeholder:opacity-0" data-te-timepicker-format24="true" id="form13" />
                    <label for="form13" class="pointer-events-none absolute top-0 left-3 mb-0 max-w-[90%] origin-[0_0] truncate pt-[0.37rem] leading-[2.15] text-neutral-500 transition-all duration-200 ease-out peer-focus:-translate-y-[1.15rem] peer-focus:scale-[0.8] peer-focus:text-primary peer-data-[te-input-state-active]:-translate-y-[1.15rem] peer-data-[te-input-state-active]:scale-[0.8] motion-reduce:transition-none dark:text-neutral-200 dark:peer-focus:text-neutral-200">Start Webinar Time</label>
                </div>
            </div>
        </div>
        <div class = "flex items-center flex-row w-full rounded-md p-3">
            <div class="p-1 text-sm text-blue-700 shadow-md rounded-lg">
                <div class="relative xl:w-96" data-te-datepicker-init data-te-input-wrapper-init>
                    <input value="{{ $course->webinar_date ?? '' }}" required name="webinar_date" type="text" class="peer block min-h-[auto] w-full rounded border-none bg-transparent py-[0.32rem] px-3 leading-[2.15] outline-none transition-all duration-200 ease-linear focus:placeholder:opacity-100 data-[te-input-state-active]:placeholder:opacity-100 motion-reduce:transition-none dark:text-neutral-200 dark:placeholder:text-neutral-200 [&:not([data-te-input-placeholder-active])]:placeholder:opacity-0" placeholder="Start Webinar Date" />
                    <label for="floatingInput" class="pointer-events-none absolute top-0 left-3 mb-0 max-w-[90%] origin-[0_0] truncate pt-[0.37rem] leading-[2.15] text-neutral-500 transition-all duration-200 ease-out peer-focus:-translate-y-[1.15rem] peer-focus:scale-[0.8] peer-focus:text-primary peer-data-[te-input-state-active]:-translate-y-[1.15rem] peer-data-[te-input-state-active]:scale-[0.8] motion-reduce:transition-none dark:text-neutral-200 dark:peer-focus:text-neutral-200">
                        Start Webinar Date
                    </label>
                </div>
            </div>
        </div>
        <div class = "flex items-center flex-row w-full rounded-md p-3">
            <div class="p-1 text-sm text-blue-700 shadow-md rounded-lg">
                <div class="relative xl:w-96" data-te-datepicker-init data-te-input-wrapper-init>
                    <input value="{{ $course->start_course ?? '' }}" required name="start_course" type="text" class="border-none peer block min-h-[auto] w-full rounded border-0 bg-transparent py-[0.32rem] px-3 leading-[2.15] outline-none transition-all duration-200 ease-linear focus:placeholder:opacity-100 data-[te-input-state-active]:placeholder:opacity-100 motion-reduce:transition-none dark:text-neutral-200 dark:placeholder:text-neutral-200 [&:not([data-te-input-placeholder-active])]:placeholder:opacity-0" placeholder="Start Course" />
                    <label for="floatingInput" class="pointer-events-none absolute top-0 left-3 mb-0 max-w-[90%] origin-[0_0] truncate pt-[0.37rem] leading-[2.15] text-neutral-500 transition-all duration-200 ease-out peer-focus:-translate-y-[1.15rem] peer-focus:scale-[0.8] peer-focus:text-primary peer-data-[te-input-state-active]:-translate-y-[1.15rem] peer-data-[te-input-state-active]:scale-[0.8] motion-reduce:transition-none dark:text-neutral-200 dark:peer-focus:text-neutral-200">
                        Start Course
                    </label>
                </div>
            </div>
        </div>
        <div class="w-full rounded-md p-3">
            <label for="default-input" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Schedule</label>
            <input required value="{{ $course->schedule ?? '' }}" name="schedule" type="text" id="default-input" class="shadow-md bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
        </div>
        @if($errors->any())
            <p class="text-sm font-light text-red-500">
                {{ $errors->first() }}
            </p>
        @endif
        <button type="submit" class="m-3 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
            Submit
        </button>
    </div>

    <div class = "w-full md:w-2/3 lg:w-2/3 p-2 ">
        <div class = "items-center flex-row w-full rounded-md p-3 ml-4">
            <div class = "p-4 lg:ml-6 mb-4 text-sm drop-shadow-lg rounded-lg shadow-md dark:text-neutral-200 dark:bg-[#1E293B]" role="alert">
                <span class = "font-medium">Webinar! </span>Date: {{ $course->webinar_date ?? '' }}, and Time: {{ $course->webinar_time ?? '' }}
            </div>
            <div class = "p-4 lg:ml-6 mb-4 text-sm rounded-lg shadow-md dark:text-neutral-200 dark:bg-[#1E293B]" role="alert">
                <span class = "font-medium">Start Course! </span> {{ $course->start_course ?? '' }}
            </div>
            <div class = "p-4 lg:ml-6 mb-4 text-sm  rounded-lg shadow-md dark:text-neutral-200 dark:bg-[#1E293B]"role="alert">
                <span class = "font-medium">Schedule! </span> {{ $course->schedule ?? '' }}
            </div>
        </div>
    </div>
</div>


