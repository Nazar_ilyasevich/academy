@extends('components.main')

@section('title', 'frontend')

@section('content')
    @include('components.sidebar')


    <div class = "content ml-6 transform ease-in-out duration-500 pt-20 px-2 md:px-5 pb-4 ">
        <form action="{{ route('frontend.store') }}" method="post">
            @csrf @method('patch')

            @include('components.partials.form')

        </form>
    </div>





    <script>
        const timepickerValue = document.querySelector("#timepicker-value"); const
            tminputValue = new te.Timepicker(timepickerValue);
        timepickerValue.addEventListener("input.te.timepicker", (input) => { const
            valueDiv = document.querySelector("#span-input-value"); valueDiv.innerText
            = input.target.value; });
    </script>

@endsection
