<?php

namespace Database\Seeders;

use Database\Seeders\Seed\CourseSeeder;
use Database\Seeders\Seed\UserSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            UserSeeder::class,
            CourseSeeder::class,
        ]);
    }

}
